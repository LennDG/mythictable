# Quick Start

Install .NET Core SDK, version 2.2 or higher from <https://dotnet.microsoft.com/download>.

Install node.js using Visual Studio Installer or from <https://nodejs.org/en/download/>.

Open a console window (`cmd` or `powershell`):

Navigate to the server project folder (`server/src/MythicTable/`) and run:
  ```
  # From server/src/MythicTable/...

  # For first-time setup only:
  dotnet dev-certs https
  dotnet dev-certs https --trust

  # Run this to start the server
  dotnet watch run
  ```

Open a new console window. Navigate to the client project fodler (`html/`) and run:
   ```
   # From html/...

   # For first-time setup, or after package dependencies have changed
   npm install

   # Run this to start the client-side development server
   npm run serve
   ```

Go to <http://localhost:5000> to interact with the application.

For Windows users, once initial setup has been completed, `Start-DevEnv.ps1` can be used to
start all development servers and open a browser page to the client automatically.

The commands above will run both the client and server in auto-compile mode, and it is not
necessary to explicitly compile the server side code using Visual Studio.  Client side
JavaScript and CSS will auto-update as changes are made, although it may be desirable refresh
as needed.

Both running processes can be stopped using `Ctrl-c`.


# Debugging the Server

To debug the server using Visual Studio, make sure that an instance is not currently running.
An instance that is running in a console window using `dotnet` can be stopped with `Ctrl-c`.

Set the Start Up project to `server/MythicTable` and `Debug -> Start Debugging`.

Requests can be made to the server directly at <https://localhost:5001/>.


# Debugging the Client

For development, the HTML/Javascript client uses facilities provided by Node.js and `vue-cli`
to enable code auto-update, as well as debugging hooks for single file Vue components that
must be compiled.

The recommended way to debug the client code is to use Chrome's built-in debugger.  Source code
belonging to compiled code can be found in the `webpack://` section of the source browser.  You
can set breakpoints and step through the code as if it were regular Javascript.
