import Asset from '../entity/Asset'

async function loadDebugScene({ vm, sessionId, sceneId}) {
    if (sessionId == '$demo' && sceneId == 'stronghold') {
        const data = await import('../debug/data_demo')
        await data.loader(vm.$store)

        Asset.loadAll(vm.$store.state.entities)

        vm.director.connect()
    }
}

export { loadDebugScene }
