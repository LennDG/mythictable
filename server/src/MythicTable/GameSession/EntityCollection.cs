﻿using Microsoft.AspNetCore.JsonPatch.Exceptions;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace MythicTable.GameSession
{
    public class EntityCollection : Dictionary<string, dynamic>, IEntityCollection
    {
        public void Add(dynamic entity)
        {
            string id = null;
            try
            {
                switch (entity.id)
                {
                    case string s:
                        id = s;
                        break;
                    case JValue value:
                        switch (value.Type)
                        {
                            case JTokenType.String: id = value.Value<string>(); break;
                            case JTokenType.Float:
                            case JTokenType.Integer:
                                id = value.Value.ToString();
                                break;
                        }
                        break;
                    default:
                        id = entity.id as string;
                        break;
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException("Entity did not contain 'id' property", nameof(entity), e);
            }

            if (id == null)
            {
                throw new ArgumentException("Entity 'id' property is not a string", nameof(entity));
            }

            this.Add(id, entity);
        }

        public Task<bool> ApplyDelta(IEnumerable<EntityOperation> operations)
        {
            foreach (var operation in operations)
            {
                if (!this.TryGetValue(operation.EntityId, out dynamic entity))
                {
                    // If entity does not exist, scaffold enough of it for patching
                    // XXX: For PROTOTYPING only.
                    // XXX: Not using JObject here due to https://github.com/aspnet/AspNetCore/issues/2429
                    entity = new ExpandoObject();
                    entity.id = operation.EntityId;

                    IDictionary<string, object> obj = entity;
                    foreach (var op in operation.Patch.Operations)
                    {
                        // XXX: Assuming we only ever see simple paths that are absolute
                        var segments = op.path.Split('/');
                        var parentSegments = segments.Length > 2
                            ? segments.Skip(1).Take(segments.Length - 2)
                            : Enumerable.Empty<string>();

                        foreach (var segment in parentSegments)
                        {
                            if (!obj.ContainsKey(segment))
                            {
                                obj[segment] = new ExpandoObject();
                            }
                            obj = (IDictionary<string, object>)obj[segment];
                        }

                        // If doing replace, the replaced object needs to exist
                        if (op.OperationType == OperationType.Replace)
                        {
                            obj[segments.Last()] = null;
                        }
                    }

                    this.Add(entity);
                }

                try
                {
                    operation.Patch.ApplyTo(entity);
                }
                catch (JsonPatchException)
                {
                    return Task.FromResult(false);
                }
            }

            return Task.FromResult(true);
        }

        public Task<IEnumerable<dynamic>> GetEntities()
        {
            return Task.FromResult(this.Values.AsEnumerable());
        }
    }
}
